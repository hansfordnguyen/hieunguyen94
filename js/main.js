/**
 * Created by Hieu.KimNguyen on 2/26/2016.
 */
"use strict";
var myApplication = (function(){
    var graphicsUtil;
    function initialize() {
        graphicsUtil = new GraphicsUtil(20);
        registerEvent();
    }
    function registerEvent() {
        document.addEventListener('click', function(event) {
            var x = event.pageX,
                y = event.pageY;
            graphicsUtil.drawRandom(x, y, 'circle');
        });
        document.addEventListener('contextmenu', function(event) {
            event.preventDefault();
        })
    }
    return {
        'init': initialize
    }
})();

myApplication.init();

/**
 * GraphicsUtil type
 * @param delay - the amount of time after clicking and before the graphic start to move
 * @constructor
 */
function GraphicsUtil(delay) {
    this.delay = delay;
    this.aliveObjects = 0;
    // Object moving time
}
// Setting up some useful constants
GraphicsUtil.RECT_TYPE = 'rect';
GraphicsUtil.CIRCLE_TYPE = 'circle';
GraphicsUtil.WINDOW_WIDTH = window.innerWidth || document.body.clientWidth;
GraphicsUtil.WINDOW_HEIGHT = window.innerHeight || document.body.clientHeight;
GraphicsUtil.RADIUS = Math.max(GraphicsUtil.WINDOW_WIDTH, GraphicsUtil.WINDOW_HEIGHT);
GraphicsUtil.DURATION = 3000;

/**
 * Draw a random-type graphics at given coordinates
 * @param x - coordinate x
 * @param y - coordinate y
 */
GraphicsUtil.prototype.drawRandom = function(x, y) {
    var type = (Math.random() < 0.5) ? GraphicsUtil.RECT_TYPE : GraphicsUtil.CIRCLE_TYPE;
    this.draw(x, y, type);
    this.aliveObjects += 1;
};

/**
 * Draw graphics on the screen by adding a customized div element
 * @param x - coordinate x
 * @param y - coordinate y
 * @param type - type of the graphics (only rectangle and circle are supported)
 */
GraphicsUtil.prototype.draw = function(x, y, type) {
    var div = document.createElement('div');
    document.body.appendChild(div);
    div.classList.add(type);
    div.style.left = x + 'px';
    div.style.top = y + 'px';
    // Set random size for the graphics based on its type
    if (type === GraphicsUtil.RECT_TYPE) {
        div.style.with = 50 + Math.random() * 200 + 'px';
        div.style.height = 50 + Math.random() * 200 + 'px';
    } else {
        div.style.width = div.style.height = 50 + Math.random() * 200 + 'px';
    }
    this.moveRandom(div, x, y);
};
/**
 * Move graphic around
 * @param div - the element to be moved
 * @param x - original coordinate x
 * @param y - original cooridnate y
 */
GraphicsUtil.prototype.moveRandom = function(div, x, y) {
    function removeHandler() {
        document.body.removeChild(div);
        this.aliveObjects -= 1;
    }

    function moveHandler() {
        var randomAngle = Math.random() * (2 * Math.PI); //  Get a random direction
        // Calculate new cooridates
        var randX = x + GraphicsUtil.RADIUS * Math.cos(randomAngle),
            randY = y + GraphicsUtil.RADIUS * Math.sin(randomAngle);
        div.style.left = randX + 'px';
        div.style.top = randY + 'px';
    }
    setTimeout(moveHandler, this.delay);
    // The elements are destroyed after finishing its path.
    setTimeout(removeHandler, GraphicsUtil.DURATION);
};